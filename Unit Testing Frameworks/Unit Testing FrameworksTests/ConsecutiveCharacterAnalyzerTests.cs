﻿using NUnit.Framework;
using CharacterAnalysis;

namespace CharacterAnalysisTests
{
    [TestFixture]
    public class ConsecutiveCharacterAnalyzerTests
    {
        [Test]
        public void TestConsecutiveIdenticalLetters()
        {
            var input = "aaabbcc";
            var expectedMax = 3;

            var result = ConsecutiveCharacterAnalyzer.GetMaxConsecutiveIdenticalLetters(input);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedMax, result);
        }

        [Test]
        public void TestConsecutiveIdenticalDigits()
        {
            var input = "11223344";
            var expectedMax = 2;

            var result = ConsecutiveCharacterAnalyzer.GetMaxConsecutiveIdenticalDigits(input);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedMax, result);
        }

        [Test]
        public void TestMixedContent()
        {
            var input = "aa1122bb";
            var expectedMaxLetters = 2;
            var expectedMaxDigits = 2;

            var resultLetters = ConsecutiveCharacterAnalyzer.GetMaxConsecutiveIdenticalLetters(input);
            var resultDigits = ConsecutiveCharacterAnalyzer.GetMaxConsecutiveIdenticalDigits(input);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedMaxLetters, resultLetters);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedMaxDigits, resultDigits);
        }

        [Test]
        public void TestEmptyInput()
        {
            var input = "";
            var expectedMax = 0;

            var resultLetters = ConsecutiveCharacterAnalyzer.GetMaxConsecutiveIdenticalLetters(input);
            var resultDigits = ConsecutiveCharacterAnalyzer.GetMaxConsecutiveIdenticalDigits(input);

            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedMax, resultLetters);
            Microsoft.VisualStudio.TestTools.UnitTesting.Assert.AreEqual(expectedMax, resultDigits);
        }
    }
}
