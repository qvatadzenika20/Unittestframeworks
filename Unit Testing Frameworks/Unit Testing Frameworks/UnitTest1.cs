using System;

namespace CharacterAnalysis
{
    public static class ConsecutiveCharacterAnalyzer
    {
        public static int GetMaxConsecutiveIdenticalLetters(string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return 0;

            int maxConsecutive = 0;
            int currentCount = 0;
            char? previousChar = null;

            foreach (var c in input)
            {
                if (char.IsLetter(c))
                {
                    if (c == previousChar)
                    {
                        currentCount++;
                    }
                    else
                    {
                        currentCount = 1;
                        previousChar = c;
                    }
                    maxConsecutive = Math.Max(maxConsecutive, currentCount);
                }
                else
                {
                    currentCount = 0;
                }
            }

            return maxConsecutive;
        }

        public static int GetMaxConsecutiveIdenticalDigits(string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return 0;

            int maxConsecutive = 0;
            int currentCount = 0;
            char? previousChar = null;

            foreach (var c in input)
            {
                if (char.IsDigit(c))
                {
                    if (c == previousChar)
                    {
                        currentCount++;
                    }
                    else
                    {
                        currentCount = 1;
                        previousChar = c;
                    }
                    maxConsecutive = Math.Max(maxConsecutive, currentCount);
                }
                else
                {
                    currentCount = 0;
                }
            }

            return maxConsecutive;
        }
    }
}
